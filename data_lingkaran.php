<?php
  require_once "class_lingkaran.php";//panggil file sebelumnya agar fungsinya bisa digunakan

  echo 'Nilai PHI = ' .Lingkaran::PHI;//ambil variabel dari class sebelumnya

  //buat variabel baru menggunakan fungsi di class sebelumnya
  $lingkar1 = new Lingkaran(10);
  $lingkar2 = new Lingkaran(4);

  //gunakan fungsi class sebelumnya untuk mendapatkan hasil
  echo "<br/>Luas Lingkaran I = ".$lingkar1->getLuas();
  echo "<br/>Luas Lingkaran II = ".$lingkar2->getLuas();

  echo "<br/>Keliling Lingkaran I = ".$lingkar1->getKeliling();
  echo "<br/>Keliling Lingkaran II = ".$lingkar2->getKeliling();
?>
