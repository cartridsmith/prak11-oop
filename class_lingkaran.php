<?php
    class Lingkaran {//buat & buka class

    //deklarasi variabel
    private $jari;
    const PHI = 3.14;
    //buat consturctor
    function __construct($r){
      $this->jari = $r;
    }
    //buat fungsi getter untuk luas dan keliling
    function getLuas(){
      return self::PHI * $this->jari * $this->jari;
    }
    function getKeliling (){
      return 2 * self::PHI * $this->jari;
    }
  } //tutup class
?>
